// Database config file

const { Pool, Client } = require('pg');

const pool = new Pool({
    user: 'postgres',
    password: 'postgres',
    database: 'josephbauer',
});

// Error handler
pool.on('error', (err, client) => {
    console.error('Unexpected errot on idle client', err)
    process.exit(-1)
})


exports.db = pool;
