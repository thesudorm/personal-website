var express = require('express');
var router = express.Router();

// Get the pool created in the database.js file
var databaseModule = require('./../database.js');
const db = databaseModule.db;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('home', { title: 'Express' });
});

/* GET login page. */
router.get('/login', function(req, res, next) {
  res.render('login');
});


/* GET blogs page. */
router.get('/blogs', function(req, res, next) {

    // Get all blog titles from database
    db.query('SELECT id, title FROM blogs')
        .then(function(data){
            console.log(data.rows);
            res.render('blogs', { blogs: data.rows });
        })
        .catch(function(err){
            console.log(err);
        })
});

/* GET a single blog and render it */
router.get('/blog', function(req, res, next) {

    console.log(req);

    // Get user input 
    var blogToGet = req.query.id;

    // Get all blog titles from database
    db.query('SELECT title, body FROM blogs WHERE id = ($1)', [blogToGet])
        .then(function(data){
            console.log(data.rows);
            res.render('blog', { blog: data.rows });
        })
        .catch(function(err){
            console.log(err);
        })
});

/* GET portfolio page. */
router.get('/portfolio', function(req, res, next) {
  res.render('portfolio');
});

/* Autheticate the credentials received from login page */
router.get('/authenticate', function(req, res, next) {

    // Logic for autheticating
    console.log(req.query);

    var username = req.query.username;
    var password = req.query.password;

    db.query("SELECT username, password FROM users WHERE username = ($1)", [username])
        .then(function(data){

            var pageToReturn;

            console.log(data);

            // If the user is found, check password
            if(data.rowCount == 1){
                if(password == data.rows[0].password) {
                    pageToReturn = 'success';
                } else {
                    pageToReturn = 'failed';
                } 
            }
            else {
                pageToReturn = 'failed';
            }

            res.render(pageToReturn);
        })
        .catch(function(err){
            console.log(err);
        })
});

module.exports = router;
