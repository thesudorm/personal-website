#!/bin/bash

USER="postgres"
DATABASE="josephbauer"

#echo "Creating database.."
#psql -U $USER -c "CREATE DATABASE " $DATABASE

echo "Making User table"
psql -U $USER -d $DATABASE -c "CREATE TABLE users (id serial, username varchar(20), password varchar(20), is_admin boolean);"

echo "Inserting Admin User"
psql -U $USER -d $DATABASE -c "INSERT INTO  users (username, password, is_admin) VALUES('bauerj', 'password', TRUE);"

echo "Making Blog Table"
psql -U $USER -d $DATABASE -c "CREATE TABLE blogs (id serial, title varchar(100), body text);"

echo "Inserting blogs into blog table"
psql -U $USER -d $DATABASE -c "INSERT INTO blogs (title, body) VALUES('Test Blog One', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');"
psql -U $USER -d $DATABASE -c "INSERT INTO blogs (title, body) VALUES('Test Blog Two', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');"
psql -U $USER -d $DATABASE -c "INSERT INTO blogs (title, body) VALUES('Test Blog Three', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');"
