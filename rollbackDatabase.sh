#!/bin/bash

USER="postgres"
DATABASE="josephbauer"

# Ensure that I meant to use this script
# Ask the user for confirmation
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo -n "WARNING!! You are about to drop all of the tables. Are you sure you meant to do this? (y/n) "
read answer
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

if [ "$answer" != "${answer#[Yy]}" ] ;then
    echo -n "Are you sure? "
    read answer
    if [ "$answer" != "${answer#[Yy]}" ] ;then
        echo "Dropping tables"
        psql -U $USER -d $DATABASE -c "DROP TABLE users" 
        psql -U $USER -d $DATABASE -c "DROP TABLE blogs" 
    else
        echo "Script cancelled"
    fi
else
    echo "Script cancelled"
fi
